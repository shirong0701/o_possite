<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Profile</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo form_open_multipart( uri_string() );?>
                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <?php echo lang('create_user_avatar_label', 'photo_file');?>
                    </div>
                    <div class="form-group">
                        <div style="display: inline-block;">
                            <nav class="pull-left" role="navigation" style="margin-right: 0px; margin-bottom: 0px;">
                                <?php
                                $photo_url = '';
                                if ( isset( $avatar_thumb ) && $avatar_thumb != '' )
                                    $photo_url = $avatar_thumb['value'];
                                else if ( isset( $avatar ) && $avatar != '' )
                                    $photo_url = $avatar['value'];
                                if( $photo_url == '' )
                                    $photo_url = base_url('assets/images/unknown.jpg');
                                ?>

                                <img id="avatar_img" src="<?php echo $photo_url ?>" style="width: 100px; height: 100px; margin-left: 0px; border: solid 1px; border-color: #ccc; border-radius: 4px;" class="pull-left">
                            </nav>
                        </div>
                        <div style="display: inline-block;">
                            <nav class="pull-left" style="position: relative; margin-bottom: 0px;">
                                <input name="avatar_file" id="avatar_file" type="file" accept="image/*" style="display:none"">
                                <input id="default_photo" type="hidden" value="<?php echo base_url('assets/images/unknown.jpg'); ?>">
                                <input id="avatar_changed" name="avatar_changed" type="hidden" value="false">
                                <?php echo form_button(array('name'=>'browse_photo','class'=>'btn btn-success', "onClick"=>"$('input[id=avatar_file]').click();"), "Browse"); ?>
                                <?php echo form_button(array('name'=>'remove_photo', 'id'=>'remove_photo', 'class'=>'btn btn-success'), "Remove"); ?>
                            </nav>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_user_fname_label', 'first_name');?>
                        <?php echo form_input($first_name, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_user_lname_label', 'last_name');?>
                        <?php echo form_input($last_name, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_user_email_label', 'email');?>
                        <?php echo form_input($email, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_user_phone_label', 'phone');?>
                        <?php echo form_input($phone, '', 'class="form-control"');?>
                    </div>
                    <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-default btn-success pull-right'), lang('edit_user_submit_btn'))?>
                    <?php echo anchor(site_url('auth/change_password'), lang('change_password_heading'), array('name'=>'submit', 'class'=>'btn btn-default btn-warning pull-left'))?>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).on("change", "#avatar_file", function( e ) {
        if( this.files[0] ) {
            $('#avatar_img')[0].src = window.URL.createObjectURL(this.files[0]);
            $('#avatar_changed').val( "true" );
        }
    });

    $(document).on("click", "#remove_photo", function( e ) {
        var default_photo = $( "#default_photo" ).attr('value');
        $('#avatar_img')[0].src = default_photo;
        $('#avatar_file').files = null;
        $('#avatar_changed').val( "true" );
    });
</script>