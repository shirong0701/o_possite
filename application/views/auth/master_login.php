<div>
  <div class="col-md-10 col-md-offset-1">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
          <div class="panel-body">
            <h2 class="panel-title">Master Log In</h2>
            <?php echo form_open("auth/master_login", array("role"=>"form"));?>
            <fieldset>
              <div id="infoMessage"><?php echo $message;?></div>
              <div class="form-group">
                <?php echo form_input($identity);?>
              </div>
              <div class="form-group">
                <?php echo form_input($password);?>
              </div>
              <div class="checkbox">
                <label>
                  <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>Remember Me
                </label>
                <a href="forgot_password" class="pull-right"><?php echo lang('login_forgot_password');?></a>
              </div>
              <!-- Change this to a button or input when using this as a form -->
              <div class="col-md-6">
                <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-lg btn-success btn-block'), lang('login_submit_btn'));?>
              </div>
              <div class="col-md-6">
                <?php echo form_button(array('name'=>'submit','class'=>'btn btn-lg btn-success btn-block', 'onClick'=>'btn_sign_up_clicked()'), lang('sign_up_btn'));?>
              </div>
            </fieldset>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>

<script>
  function btn_sign_up_clicked()
  {
    window.location.href = "create_user";
  }
</script>