<div>
<div class="col-md-9 col-md-offset-1">
      <div class="col-md-5 col-md-offset-4">
        <div class="login-panel panel panel-default">
          <div class="panel-body">
            <h2 class="panel-title">Staff Log In</h2>
            <?php echo form_open("auth/staff_login", array("role"=>"form"));?>
            <fieldset style="text-align: center">
              <div id="infoMessage"><?php echo $message;?></div>
              <div class="form-group">
                <?php echo lang('create_user_code_label', 'password');?>
                <?php echo form_input( $code1, '', 'class="form-control user-code"');?>
                <?php echo form_input( $code2, '', 'class="form-control user-code"');?>
                <?php echo form_input( $code3, '', 'class="form-control user-code"');?>
                <?php echo form_input( $code4, '', 'class="form-control user-code"');?>
              </div>

              <div class="checkbox">
                  <label>
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>Remember Me
                  </label>
              </div>
              <!-- Change this to a button or input when using this as a form -->
              <div class="col-md-6 col-md-offset-3">
                <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-lg btn-success btn-block'), lang('login_submit_btn'));?>
              </div>
            </fieldset>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>

<script>
  $("#code1").keyup(function(){
    if($(this).val().length == 1)
    {
      $("#code2").focus();
    }
  });

  $("#code2").keyup(function(){
    if($(this).val().length == 1)
    {
      $("#code3").focus();
    }
  });

  $("#code3").keyup(function(){
    if($(this).val().length == 1)
    {
      $("#code4").focus();
    }
  });
</script>