<?php
/**
* Name:  staff_list
*
* Author: Tiger
*
* Created:  02.05.2015 4:22 AM
*
* Requirements: PHP5 or above
*
*/
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('index_heading');?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo lang('index_subheading');?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-staffs">
                            <thead>
                            <tr>
                                <th><?php echo lang('index_photo_th');?></th>
                                <th><?php echo lang('index_fname_th');?></th>
                                <th><?php echo lang('index_lname_th');?></th>
                                <th><?php echo lang('index_email_th');?></th>
                                <th><?php echo lang('index_phone_th');?></th>
                                <th><?php echo lang('index_code_th');?></th>
                                <th><?php echo lang('index_status_th');?></th>
                                <th><?php echo lang('index_action_th');?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($users) > 0)
                            {
                                foreach ($users as $user):?>
                                    <tr>
                                        <td align="center">
                                            <?php
                                            $photo_url = '';
                                            if ( isset( $user->avatar_thumb ) && $user->avatar_thumb != '' )
                                                $photo_url = $user->avatar_thumb;
                                            else if ( isset( $user->avatar ) && $user->avatar != '' )
                                                $photo_url = $user->avatar;
                                            else
                                                $photo_url = base_url('assets/images/unknown.jpg');
                                            ?>

                                            <img src="<?php echo $photo_url;?>" height="30">
                                        </td>
                                        <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                                        <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                        <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                                        <td><?php echo htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8');?></td>
                                        <td><?php echo htmlspecialchars($user->code,ENT_QUOTES,'UTF-8');?></td>
                                        <td id="id_<?php echo $user->id; ?>">
                                            <?php echo ($user->active) ? '<a action="Deactivate" user_id="'.$user->id.'" data-href="'.site_url('auth/deactivate/'.$user->id).'" class="btn btn-xs btn-success activate_confirm">Activated</a>' : '<a user_id="'.$user->id.'" action="Activate" data-href="'.site_url('auth/activate/'.$user->id).'" class="btn btn-xs btn-warning activate_confirm">Inactivated</a>';?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                    Actions
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('auth/edit_staff/'.$user->id);?>">Edit</a></li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('auth/delete_staff/'.$user->id)?>">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<style>
    .class-with-width { width: 350px !important; }
</style>
<script>
    $(document).ready(function() {
        $('#dataTables-staffs').DataTable({
            responsive: true
        });
    });

    $(document).on("click", ".delete_confirm", function( e ) {
        var exec_url = $( this ).attr( 'data-href' );
//        window.alert( exec_url );
        bootbox.confirm({
            title: 'danger - danger - danger',
            message: 'Are you sure you want to delete this User?',
            animate: true,
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-default'
                },
                'confirm': {
                    label: 'Delete',
                    className: 'btn-danger pull-right'
                }
            },
            callback: function(result) {
                if ( result ) {
                    window.location.href = exec_url;
                }
            }
        });
    });

    $(document).on("click", ".activate_confirm", function( e ) {
        var exec_url = $( this ).attr( 'data-href' );
        var user_id = $( this).attr( 'user_id' );
        console.log( document.getElementById("id_"+user_id).innerHTML );
        var action = $( this ).attr('action');
//        window.alert( $( this ).html() );
//        $( this ).toggleClass('btn-success',false);
//        $( this ).toggleClass('btn-warning',true);

        var className;
        if( action == "Activate" )
            className = 'btn-success pull-right';
        else
            className = 'btn-warning pull-right';

        bootbox.confirm({
            title: action + " User",
            message: 'Are you sure you want to ' + action + ' this User?',
            animate: true,
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-default'
                },
                'confirm': {
                    label: action,
                    className: className
                }
            },
            callback: function( result ) {
                if ( result ) {
                    $.post( exec_url,
                        function( data, textStatus, jqXHR )
                        {
                            if( textStatus == 'success' )
                            {
                                if( action == "Activate" )
                                    document.getElementById( "id_"+ user_id ).innerHTML = "<a action='Deactivate' user_id='" + user_id + "' data-href='http://localhost/possite/index.php/auth/deactivate/" + user_id + "' class='btn btn-xs btn-success activate_confirm'>Activated</a>";
                                else
                                    document.getElementById( "id_"+ user_id ).innerHTML = "<a action='Activate' user_id='" + user_id + "' data-href='http://localhost/possite/index.php/auth/activate/" + user_id + "' class='btn btn-xs btn-warning activate_confirm'>Inactivated</a>";
                            }
                            else
                            {
                                bootbox.dialog({
                                    title: action + " Error!",
                                    message: "Cannot " + action + " This User",
                                    buttons: {
                                        main: {
                                            label: "I Know!",
                                            className: "btn-primary",
                                            callback: function() {
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    );
                }
            }
        });
    });

</script>