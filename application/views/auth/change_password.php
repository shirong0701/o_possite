<div id="page-wrapper">
      <div class="row">
            <div class="col-lg-12">
                  <h1 class="page-header"><?php echo lang('change_password_heading');?></h1>
            </div>
            <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
            <div class="col-lg-6">
                  <div class="panel panel-default">
                        <div class="panel-heading">
                              <?php echo lang('change_password_heading');?>
                        </div>
                        <div class="panel-body">
                              <?php echo form_open("auth/change_password", array("role"=>"form"));?>
                              <div id="infoMessage"><?php echo $message;?></div>
                              <div class="form-group">
                                    <?php echo lang('change_password_old_password_label', 'old_password');?>
                                    <?php echo form_input($old_password, '', ' class="form-control"');?>
                              </div>
                              <div class="form-group">
                                    <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
                                    <?php echo form_input($new_password, '', ' class="form-control"');?>
                              </div>
                              <div class="form-group">
                                    <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
                                    <?php echo form_input($new_password_confirm, '', ' class="form-control"');?>
                              </div>
                              <?php echo form_input($user_id);?>
                              <?php echo form_submit(array('name'=>'submit', 'class'=>' btn btn-default btn-success'), lang('change_password_submit_btn'));?>
                              <?php echo form_close();?>
                        </div>
                  </div>
            </div>
      </div>
</div>