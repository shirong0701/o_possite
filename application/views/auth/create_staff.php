<div id="page-wrapper">
      <div class="row">
            <h1 class="page-header"><?php echo lang('create_staff_heading');?></h1>
      </div>

      <div class="panel-default" style="margin-top: 0px;">
            <div class="col-md-5">
                  <?php echo form_open_multipart( "auth/create_staff" );?>
                  <div id="infoMessage"><?php echo $message;?></div>
                  <div class="">
                        <?php echo lang('create_user_avatar_label', 'photo_file');?>
                  </div>
                  <div class="form-group">
                        <div style="display: inline-block;">
                              <nav class="pull-left" role="navigation" style="margin-right: 0px; margin-bottom: 0px;">
                                    <img id="avatar_img" src="<?php echo IMG_DIR ?>/unknown.jpg" style="width: 100px; height: 100px; margin-left: 0px; border: solid 1px; border-color: #ccc; border-radius: 4px;" class="pull-left">
                              </nav>
                        </div>
                        <div style="display: inline-block;">
                              <nav class="pull-left" style="position: relative; margin-bottom: 0px;">
                                    <input type="file" accept="image/*" style="display:none" id="avatar_file" name="avatar_file">
                                    <?php echo form_button(array('name'=>'submit','class'=>'btn btn-success', "onClick"=>"$('input[id=avatar_file]').click();"), "Browse"); ?>
                              </nav>
                        </div>
                  </div>
                  <div class="form-group">
                        <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                        <?php echo form_input($first_name, '', 'class="form-control"');?>
                  </div>

                  <div class="form-group">
                        <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                        <?php echo form_input($last_name, '', 'class="form-control"');?>
                  </div>
                  <div class="form-group">
                        <?php echo lang('create_user_email_label', 'email');?> <br />
                        <?php echo form_input($email, '', 'class="form-control"');?>
                  </div>

                  <div class="form-group">
                        <?php echo lang('create_user_phone_label', 'phone');?> <br />
                        <?php echo form_input($phone, '', 'class="form-control"');?>
                  </div>

                  <div class="form-group">
                        <?php echo lang('create_user_code_label', 'password');?>
                        <?php echo form_input($code1, '', 'class="form-control user-code"');?>
                        <?php echo form_input($code2, '', 'class="form-control user-code"');?>
                        <?php echo form_input($code3, '', 'class="form-control user-code"');?>
                        <?php echo form_input($code4, '', 'class="form-control user-code"');?>
                  </div>
                  <?php echo form_button(array('name'=>'cancel_button', 'id'=>'cancel_button', 'class'=>'btn btn-default btn-success pull-left'), lang('cancel_button_label'));?>
                  <p><?php echo form_submit( array( 'submit', 'class'=>'btn btn-default btn-success pull-right'), lang('create_user_submit_btn') );?></p>

                  <?php echo form_close();?>
            </div>
      </div>
</div>

<script>
      $(document).on("change", "#avatar_file", function( e ) {
            if( this.files[0] )
                  $('#avatar_img')[0].src = window.URL.createObjectURL( this.files[0] );
      });

      $(function(){
            $("#code1").keyup(function(){
                  if($(this).val().length == 1)
                  {
                        $("#code2").focus();
                  }
            });

            $("#code2").keyup(function(){
                  if($(this).val().length == 1)
                  {
                        $("#code3").focus();
                  }
            });

            $("#code3").keyup(function(){
                  if($(this).val().length == 1)
                  {
                        $("#code4").focus();
                  }
            });
            $("#cancel_button").click( function() {
                  window.history.back( -1 );
            });
      })
</script>