<?php ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PosSite</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo ASSETS_DIR;?>/components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- DateTimePicker Plugin CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- Calendar CSS -->
    <link rel="stylesheet" href="<?php echo ASSETS_DIR;?>/components/calendar/css/calendar.css">

    <!-- Custom Fonts -->
    <link href="<?php echo ASSETS_DIR;?>/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo ASSETS_DIR;?>/components/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/bootstrap/js/bootstrap.min.js"></script>

    <!-- BootBox -->
    <script src="<?php echo ASSETS_DIR;?>/components/bootstrap/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/metisMenu/metisMenu.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo ASSETS_DIR;?>/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Bootstrap DateTimePicker Plugin -->
    <script src="<?php echo ASSETS_DIR;?>/components/moment/js/moment.min.js"></script>
    <script src="<?php echo ASSETS_DIR;?>/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo JS_DIR;?>/sb-admin-2.js"></script>

    <!-- Custom CSS -->
    <link href="<?php echo CSS_DIR;?>/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo CSS_DIR;?>/style.css" rel="stylesheet">

</head>
<script>
    var jssor_slider2 = new $JssorSlider$("slider2_container", options);
    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizes
    function ScaleSlider() {
        var parentWidth = jssor_slider2.$Elmt.parentNode.clientWidth;
        if (parentWidth)
        jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
        else
        window.setTimeout(ScaleSlider, 30);
    }
    ScaleSlider();
</script>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background: url(<?php echo IMG_DIR ?>/header_back.png); height: 120px;">
        <div id="slider2_container" class="navbar-header" style="position: absolute; left: 0px; bottom: 0px; width: 600px; margin: 0px auto; height: 91px;">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="<?php echo IMG_DIR ?>/mark.png" align="bottom">
        </div>

        <div class="navbar-header navbar-right" style="position: absolute; bottom: 0px; right: 0;">

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a class="dropdown-toggle nav_font_style" href="<?php echo site_url('main'); ?>">
                    <i class="fa fa-home nav_icon_size"></i> Home
                </a>
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle nav_font_style" data-toggle="dropdown" href="#">
                    <i class="fa fa-user nav_icon_size"></i> About Us
                </a>
            </li>
            <!-- /.dropdown -->
<?php
    $this->load->library( 'ion_auth' );
    if ( $this->ion_auth->logged_in() ) {
        $current_user       = $this->ion_auth->user()->row();
        $current_user_name  = $current_user->username;

        $photo_url = '';
        if ( isset( $current_user->avatar_thumb ) && $current_user->avatar_thumb != '' )
            $photo_url = $current_user->avatar_thumb;
        else if ( isset( $current_user->avatar ) && $current_user->avatar != '' )
            $photo_url = $current_user->avatar;
        else
            $photo_url = base_url('assets/images/unknown.jpg');
?>
        <li class="dropdown">
            <a class="dropdown-toggle nav_font_style" data-toggle="dropdown" href="#">
                <img src="<?php echo $photo_url;?>" class="img-circle" height="30">
                <?php echo $current_user_name; ?> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo site_url('auth/profile');?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="<?php echo site_url('auth/settings')?>"><i class="fa fa-gear fa-fw"></i>
                        Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo site_url('auth/logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
<?php
    }
    else
    {
?>
            <li class="dropdown">
                <a class="dropdown-toggle nav_font_style" data-toggle="dropdown" href="#">
                    <i class="fa fa-sign-in"></i> Log In <i class="fa fa-caret-down "></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?php echo site_url('auth/master_login');?>"><i class="fa fa-user fa-fw"></i>Master</a>
                    </li>
                    <li><a href="<?php echo site_url('auth/staff_login')?>"><i class="fa fa-user fa-fw"></i>
                            Staff</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
<?php
    }
?>
        </div>
        <!-- /.navbar-top-links -->
    </nav>

</div>

