<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Profile</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Profile
                </div>
                <div class="panel-body">
                    <?php echo form_open(uri_string(), array("role"=>"form"));?>
                        <div id="infoMessage"><?php echo $message;?></div>
                        <div class="form-group">
                            <?php echo lang('edit_user_fname_label', 'first_name');?>
                            <?php echo form_input($first_name, '', 'class="form-control"');?>
                        </div>
                        <div class="form-group">
                            <?php echo lang('edit_user_lname_label', 'last_name');?>
                            <?php echo form_input($last_name, '', 'class="form-control"');?>
                        </div>
                        <div class="form-group">
                            <?php echo lang('edit_user_email_label', 'email');?>
                            <?php echo form_input($email, '', 'class="form-control"');?>
                        </div>
                        <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-default btn-success pull-right'), lang('edit_user_submit_btn'))?>
                        <?php echo anchor(site_url('auth/change_password'), lang('change_password_heading'), array('name'=>'submit', 'class'=>'btn btn-default btn-warning pull-left'))?>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>