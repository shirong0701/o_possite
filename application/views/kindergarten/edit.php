<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Kindergarten</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo form_open(uri_string(), array("role"=>"form"));?>
                    <div id="infoMessage"><?php echo $message;?></div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_name_label', 'kindergarten_name');?>
                        <?php echo form_input($kindergarten_name, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_email_label', 'email');?>
                        <?php echo form_input($email, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_phone_label', 'phone');?>
                        <?php echo form_input($phone, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_address_label', 'address');?>
                        <?php echo form_input($address, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_address2_label', 'address2');?>
                        <?php echo form_input($address2, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_city_label', 'city');?>
                        <?php echo form_input($city, '', 'class="form-control"');?>
                    </div>
                    <div class="form-group">
                        <?php echo lang('edit_kindergarten_zipcode_label', 'zipcode');?>
                        <?php echo form_input($zipcode, '', 'class="form-control"');?>
                    </div>
                    <button class="btn btn-default btn-success pull-right"><?php echo lang('edit_kindergarten_submit_btn');?></button>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>