<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">My Kindergarten<a href="<?php echo site_url('kindergarten/mykindergarten/edit');?>" class="btn btn-success btn-sm pull-right">Edit</a></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="list-group">
                <div class="list-group-item clearfix">
                    <div class="kindergarten-info-label col-xs-3">Name </div><div class="col-xs-8"><?php echo $kindergarten->name;?></div>
                </div>
                <div class="list-group-item clearfix">
                    <div class="kindergarten-info-label col-xs-3">E-Mail</div><div class="col-xs-8"><?php echo $kindergarten->email;?></div>
                </div>
                <div class="list-group-item clearfix">
                    <div class="kindergarten-info-label col-xs-3">Phone</div><div class="col-xs-8"><?php echo $kindergarten->phone;?></div>
                </div>
                <div class="list-group-item clearfix">
                    <div class="kindergarten-info-label col-xs-3">Address</div><div class="col-xs-8"></div>
                </div>
                <div class="list-group-item disabled clearfix">
                    <div class="kindergarten-info-label col-xs-3">Max Children</div><div class="col-xs-8"><?php echo $kindergarten->max_num_children;?></div>
                </div>
                <div class="list-group-item disabled clearfix">
                    <div class="kindergarten-info-label col-xs-3">Started At </div><div class="col-xs-8"><?php echo $kindergarten->started_at;?></div>
                </div>
                <div class="list-group-item disabled clearfix">
                    <div class="kindergarten-info-label col-xs-3">Ended At </div><div class="col-xs-8"><?php echo $kindergarten->ended_at;?></div>
                </div>
            </div>
        </div>
    </div>
</div>