<?php
$curPage = $control.'/'.$action;

$sidebar_menus = array(
    //array('#', 'Dashboard', 'fa fa-dashboard fa-fw'),
    array('#', 'Kindergartens', 'fa fa-sitemap fa-fw',
        array(
            array('kindergarten/index', 'List'),
            array('kindergarten/create', 'Add New')
        )
    )
);
?>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <!--
            <li>
                <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            -->
            <?php
                foreach($sidebar_menus as $menu_item)
                {
                    $active = $menu_str = $subMenu_str = "";

                    if ($menu_item[0] == $curPage) $active = ' class="active"';

                    if ( count($menu_item) > 3 )
                    {
                        $menu_str = '<a href="'.site_url($menu_item[0]).'" '.$active.'><i class="'.$menu_item[2].'"></i>'.$menu_item[1].'<span class="fa arrow"></span></a>';
                        foreach($menu_item[3] as $submenu_item)
                        {
                            $sub_active = "";
                            if ($submenu_item[0] == $curPage) {
                                $sub_active = $active = ' class="active"';
                            }
                            $subMenu_str .= '<li><a href="'.site_url($submenu_item[0]).'"'.$sub_active.'>'.$submenu_item[1].'</a></li>';
                        }
                        if ( $active != '' )
                            $menu_str .= '<ul class="nav nav-second-level collapse in">';
                        else
                            $menu_str .= '<ul class="nav nav-second-level">';
                        $menu_str .= $subMenu_str;
                        $menu_str .= '</ul>';
                    }
                    else
                    {
                        $menu_str = '<a href="'.site_url($menu_item[0]).'"><i class="'.$menu_item[2].'"></i>'.$menu_item[1].'</a>';
                    }
                    ?>
                    <li <?php echo $active;?>><?php echo $menu_str; ?></li>
            <?php
                }
            ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
