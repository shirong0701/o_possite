<?php ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kindergarten</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- DateTimePicker Plugin CSS -->
    <link href="<?php echo ASSETS_DIR;?>/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo CSS_DIR;?>/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo CSS_DIR;?>/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo ASSETS_DIR;?>/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo ASSETS_DIR;?>/components/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/metisMenu/metisMenu.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo ASSETS_DIR;?>/components/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo ASSETS_DIR;?>/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Bootstrap DateTimePicker Plugin -->
    <script src="<?php echo ASSETS_DIR;?>/components/moment/js/moment.min.js"></script>
    <script src="<?php echo ASSETS_DIR;?>/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo JS_DIR;?>/sb-admin-2.js"></script>

</head>
<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Kindergarten</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?php echo site_url('auth/profile');?>"><i class="fa fa-user fa-fw"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo site_url('auth/logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
    </nav>



