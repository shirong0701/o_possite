<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Kindergarten</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Kindergarten
                </div>
                <div class="panel-body">
                    <?php echo form_open("kindergarten/create", array("role"=>"form"));?>
                    <div class="row">
                        <div id="infoMessage" class="col-lg-12"><?php echo $message;?></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_name_label', 'kindergarten_name');?>
                                <?php echo form_input($kindergarten_name, '', 'class="form-control"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_max_num_children_label', 'max_num_children');?>
                                <?php echo form_input($max_num_children, '', 'class="form-control"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_started_at_label', 'started_at');?>
                                <?php echo form_input($started_at, '', 'class="form-control" data-date-format="YYYY-MM-DD"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_ended_at_label', 'ended_at');?>
                                <?php echo form_input($ended_at, '', 'class="form-control" data-date-format="YYYY-MM-DD"');?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_admin_fname_label', 'admin_first_name');?>
                                <?php echo form_input($admin_first_name, '', 'class="form-control"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_admin_lname_label', 'admin_last_name');?>
                                <?php echo form_input($admin_last_name, '', 'class="form-control"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_admin_email_label', 'admin_email');?>
                                <?php echo form_input($admin_email, '', 'class="form-control"');?>
                            </div>
                            <div class="form-group">
                                <?php echo lang('create_kindergarten_admin_phone_label', 'admin_phone');?>
                                <?php echo form_input($admin_phone, '', 'class="form-control"');?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-default btn-success pull-right'), lang('create_kindergarten_submit_btn'))?>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#started_at').datetimepicker({pickTime: false});
        $('#ended_at').datetimepicker({pickTime: false});
    });
</script>
