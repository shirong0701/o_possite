<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Kindergartens</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Kindergartens
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-kindergartens">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Maximum Children</th>
                                <th>Started At</th>
                                <th>Ended At</th>
                                <th>Admin User</th>
                                <th>Admin Email</th>
                                <th>Admin Phone</th>
                                <th>Active</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($kindergartens) > 0)
                            {
                                foreach($kindergartens as $kindergarten)
                                {
                                ?>
                                    <tr class="">
                                        <td><?php echo $kindergarten->name;?></td>
                                        <td><?php echo $kindergarten->max_num_children;?></td>
                                        <td><?php echo $kindergarten->started_at;?></td>
                                        <td><?php echo $kindergarten->ended_at;?></td>
                                        <td><?php echo $kindergarten->admin_user->first_name." ".$kindergarten->admin_user->last_name;?></td>
                                        <td><?php echo $kindergarten->admin_user->email;?></td>
                                        <td><?php echo $kindergarten->admin_user->phone;?></td>
                                        <td><?php echo ($kindergarten->admin_user->active) ? '<a href="'.site_url('auth/deactivate/'.$kindergarten->admin_user->id).'" class="btn btn-xs btn-success">Activated</a>' : '<a href="'.site_url('auth/activate/'.$kindergarten->admin_user->id).'" class="btn btn-xs btn-warning">Not Activated</a>';?></td>
                                        <td>
                                            <div class="dropdown list-actions">
                                                <button class="btn btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                    Actions
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Edit</a></li>-->
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('kindergarten/delete_kindergarten/'.$kindergarten->id)?>">Delete</a></li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url('auth/resendactivate/'.$kindergarten->admin_user->id)?>">Resend Activation</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            else
                            {

                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#dataTables-kindergartens').DataTable({
            responsive: true
        });
    });
</script>
