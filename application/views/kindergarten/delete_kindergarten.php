<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('delete_kindergarten_heading');?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo form_open("kindergarten/delete_kindergarten/".$kindergarten->id);?>
                    <div class="form-group"><?php echo sprintf(lang('delete_kindergarten_subheading'), $kindergarten->name);?></div>
                    <div class="form-group">
                        <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
                        <input type="radio" name="confirm" value="yes" checked="checked" />
                        <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
                        <input type="radio" name="confirm" value="no" />
                    </div>
                    <?php echo form_hidden($csrf); ?>
                    <?php echo form_hidden(array('id'=>$kindergarten->id)); ?>
                    <p><?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-default'), lang('deactivate_submit_btn'));?></p>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>