<?php
$curPage = $control.'/'.$action;

$sidebar_menus = array(
    array( '#', 'Dashboard', 'fa fa-dashboard fa-fw' ),
    array( 'auth/profile', 'My Profile', 'fa fa-users fa-fw' ),
    array('#', 'Staffs', 'fa fa-user fa-fw',
        array(
            array( 'auth/staff_list', 'Staffs List', 'fa fa-list fa-fw' ),
            array( 'auth/create_staff', 'Add Staff', 'fa fa-user-plus fa-fw' )
        )
    ),
    array('#', 'Items', 'fa fa-sitemap fa-fw',
        array(
            array( 'categories', 'Categories', 'fa fa-university fa-fw' ),
            array( 'items', 'Item Library', 'fa fa-list fa-fw' ),
            array( 'ingredients', 'Ingredients', 'fa fa-cart-plus fa-fw' ),
            array( 'modifiers', 'Modifiers', 'fa fa-exchange fa-fw' ),
            array( 'taxes', 'Taxes', 'fa fa-book fa-fw' )
        )
    ),
    array('#', 'Sales', 'fa fa fa-print fa-fw',
        array(
            array( 'sales/list', 'List', 'fa fa-list fa-fw' ),
            array( 'sales/date', 'Date', 'fa fa-university fa-fw' )
        )
    ),
    array('#', 'Reports', 'fa fa fa-print fa-fw',
        array(
            array( 'reports/date', 'Date', 'fa fa-university fa-fw' ),
            array( 'reports/items', 'Item', 'fa fa-list fa-fw' )
        )
    )
);
?>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <!--
            <li>
                <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            -->
            <?php

            $this->load->library( 'ion_auth' );
//            if(  $this->ion_auth->is_master() )
//                echo "master";
//            else
//                echo "no master";
            foreach( $sidebar_menus as $menu_item )
            {
                $active = $menu_str = $subMenu_str = "";

                if( $menu_item[1] == "Users" )
                    if(  !$this->ion_auth->is_master() )
                        continue;

                if ( $menu_item[0] == $curPage ) $active = ' class="active"';

                if ( count($menu_item) > 3 )
                {
                    $menu_str = '<a href="'.site_url($menu_item[0]).'" '.$active.'><i class="'.$menu_item[2].'"></i>'.$menu_item[1].'<span class="fa arrow"></span></a>';
                    foreach( $menu_item[3] as $submenu_item )
                    {
                        $sub_active = "";
                        if ($submenu_item[0] == $curPage) {
                            $sub_active = $active = ' class="active"';
                        }
                        $subMenu_str .= '<li><a href="'.site_url($submenu_item[0]).'"'.$sub_active.'>';
                        if ( isset($submenu_item[2]) && $submenu_item[2] != '')
                            $subMenu_str .= '<i class="'.$submenu_item[2].'"></i>';
                        $subMenu_str .= $submenu_item[1].'</a></li>';
                    }
                    if ( $active != '' )
                        $menu_str .= '<ul class="nav nav-second-level collapse in">';
                    else
                        $menu_str .= '<ul class="nav nav-second-level">';
                    $menu_str .= $subMenu_str;
                    $menu_str .= '</ul>';
                }
                else
                {
                    $menu_str = '<a href="'.site_url($menu_item[0]).'"><i class="'.$menu_item[2].'"></i>'.$menu_item[1].'</a>';
                }
                ?>
                <li <?php echo $active;?>><?php echo $menu_str; ?></li>
            <?php
            }
            ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
