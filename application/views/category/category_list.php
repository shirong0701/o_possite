<?php
/**
 * Name:  category_list
 *
 * Author: Tiger
 *
 * Created:  02.05.2015 10:41 AM
 *
 * Requirements: PHP5 or above
 *
 */
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('index_heading');?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <table class="table table-striped table-bordered table-hover" id="table-input-categories">
                    <thead>
                    <tr>
                        <th><?php echo lang('index_name_th');?></th>
                        <th><?php echo lang('index_items_count_th');?></th>
                        <th><?php echo lang('index_action_th');?></th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo lang('index_subheading');?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">

                        <table class="table table-striped table-bordered table-hover" id="dataTables-categories">
                            <thead>
                            <tr>
                                <th width="20%"><?php echo lang('index_name_th');?></th>
                                <th style="text-align: right;" ><?php echo lang('index_items_count_th');?></th>
                                <th width="10%"><?php echo lang('index_action_th');?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <td>asdfasf</td>
                            <td align="right">0 Items</td>
                            <td align="right">asdfasf</td>
                            </tr>
                            <?php
                            if (count( $categories ) > 0)
                            {
                                foreach ( $categories as $category ):?>
                                    <tr>
                                        <td><?php echo htmlspecialchars( $category->name ,ENT_QUOTES,'UTF-8');?></td>
                                        <td><?php echo htmlspecialchars( $category->itmes_count ,ENT_QUOTES,'UTF-8');?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                    Actions
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url( 'categories/edit_category/'.$category->id );?>">Edit</a></li>
                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url( 'categories/delete_category/'.$category->id )?>">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#dataTables-categories').DataTable({
            responsive: true
        });
        $('#table-input-categories').uiTableFilter({

        });
    });

    $(function() {
        var theTable = $('#table-input-categories')

        theTable.find("tbody > tr").find("td:eq(1)").mousedown(function(){
            $(this).prev().find(":checkbox").click()
        });

        $("#filter").keyup(function() {
            $.uiTableFilter( theTable, this.value );
        })

        $('#filter-form').submit(function(){
            theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
            return false;
        }).focus(); //Give focus to input field
    });

</script>