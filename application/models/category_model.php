<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Category Model
 *
 * Author:  Tiger
 *
 * Created:  02.05.2015 7:23 AM
 *
 * Requirements: PHP5 or above
 *
 */

class Category_model extends CI_Model
{
    /**
     * Holds an array of tables used
     *
     * @var array
     **/
    public $tables = array();

    /**
     * Where
     *
     * @var array
     **/
    public $_where = array();

    /**
     * Select
     *
     * @var array
     **/
    public $_select = array();

    /**
     * Like
     *
     * @var array
     **/
    public $_like = array();

    /**
     * Limit
     *
     * @var string
     **/
    public $_limit = NULL;

    /**
     * Offset
     *
     * @var string
     **/
    public $_offset = NULL;

    /**
     * Order By
     *
     * @var string
     **/
    public $_order_by = NULL;

    /**
     * Order
     *
     * @var string
     **/
    public $_order = NULL;

    /**
     * Hooks
     *
     * @var object
     **/
    protected $_hooks;

    /**
     * Response
     *
     * @var string
     **/
    protected $response = NULL;

    /**
     * message (uses lang file)
     *
     * @var string
     **/
    protected $messages;

    /**
     * error message (uses lang file)
     *
     * @var string
     **/
    protected $errors;

    /**
     * error start delimiter
     *
     * @var string
     **/
    protected $error_start_delimiter;

    /**
     * error end delimiter
     *
     * @var string
     **/
    protected $error_end_delimiter;

    /**
     * caching of users and their groups
     *
     * @var array
     **/
    public $_cache_user_in_group = array();

    /**
     * caching of groups
     *
     * @var array
     **/
    protected $_cache_groups = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->config('category', TRUE);

        //initialize db tables data
        $this->tables  = $this->config->item('tables', 'category');
//        $this->join	   = $this->config->item('join', 'kindergarten');


        //initialize messages and error
        $this->messages    = array();
        $this->errors      = array();

        $delimiters_source = $this->config->item('delimiters_source', 'category');

        //load the error delimeters either from the config file or use what's been supplied to form validation
        if ($delimiters_source === 'form_validation')
        {
            //load in delimiters from form_validation
            //to keep this simple we'll load the value using reflection since these properties are protected
            $form_validation_class = new ReflectionClass("CI_Form_validation");

            $error_prefix = $form_validation_class->getProperty("_error_prefix");
            $error_prefix->setAccessible(TRUE);
            $this->error_start_delimiter = $error_prefix->getValue($this->form_validation);
            $this->message_start_delimiter = $this->error_start_delimiter;

            $error_suffix = $form_validation_class->getProperty("_error_suffix");
            $error_suffix->setAccessible(TRUE);
            $this->error_end_delimiter = $error_suffix->getValue($this->form_validation);
            $this->message_end_delimiter = $this->error_end_delimiter;
        }
        else
        {
            //use delimiters from config
            $this->message_start_delimiter = $this->config->item('message_start_delimiter', 'category');
            $this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'category');
            $this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'category');
            $this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'category');
        }


        //initialize our hooks object
        $this->_hooks = new stdClass;

        $this->trigger_events('model_constructor');
    }

    /**
     * register
     *
     * @return bool
     * @author Mathew
     **/
    public function register( $name, $additional_data = array() )
    {
        $this->trigger_events('pre_kindergarten_register');

        if ($this->name_check( $name ) )
        {
            $this->set_error( 'Category_already_exists' );
            return FALSE;
        }

        // Kindergartens table.
        $data = array(
            'name'   => $name
        );

        //filter out any data passed that doesnt have a matching column in the users table
        //and merge the set user data and the additional data
        $kindergarten_data = array_merge($this->_filter_data($this->tables['kindergartens'], $additional_data), $data);

        $this->trigger_events('extra_set');

        $this->db->insert($this->tables['kindergartens'], $kindergarten_data);

        $id = $this->db->insert_id();

        $this->trigger_events('post_kindergarten_register');

        return (isset($id)) ? $id : FALSE;
    }


    /**
     * categories
     *
     * @return object Kindergartens
     * @author Mingjie
     **/
    public function categories()
    {
        $this->trigger_events('categories');

        if (isset($this->_select) && !empty($this->_select))
        {
            foreach ($this->_select as $select)
            {
                $this->db->select($select);
            }

            $this->_select = array();
        }
        else
        {
            //default selects
            $this->db->select(array(
                $this->tables['categories'].'.*'
            ));
        }

        $this->trigger_events('extra_where');

        //run each where that was passed
        if (isset($this->_where) && !empty($this->_where))
        {
            foreach ($this->_where as $where)
            {
                $this->db->where($where);
            }

            $this->_where = array();
        }

        if (isset($this->_like) && !empty($this->_like))
        {
            foreach ($this->_like as $like)
            {
                $this->db->or_like($like);
            }

            $this->_like = array();
        }

        if (isset($this->_limit) && isset($this->_offset))
        {
            $this->db->limit($this->_limit, $this->_offset);

            $this->_limit  = NULL;
            $this->_offset = NULL;
        }
        else if (isset($this->_limit))
        {
            $this->db->limit($this->_limit);

            $this->_limit  = NULL;
        }

        //set the order
        if (isset($this->_order_by) && isset($this->_order))
        {
            $this->db->order_by($this->_order_by, $this->_order);

            $this->_order    = NULL;
            $this->_order_by = NULL;
        }

        $this->response = $this->db->get($this->tables['categories']);

        return $this;
    }

    /**
     * kindergarten
     *
     * @return object
     * @author Mingjie
     **/
    public function kindergarten($id = NULL)
    {
        $this->trigger_events('kindergarten');

        //if no id was passed use the current users id
        $id || $id = $this->session->userdata('user_id');

        $this->limit(1);
        $this->order_by('id', 'desc');
        $this->where($this->tables['kindergartens'].'.id', $id);

        $this->kindergartens();

        return $this;
    }

    /**
     * get_admin
     *
     * @return array
     **/
    public function get_admin($id=FALSE)
    {
        $this->trigger_events('get_admin');

        //if no id was passed use the current users id
        if ( !$id )
            return false;

        return $this->db->select($this->tables['users'].'.*')
            ->where(array($this->tables['users'].'.'.$this->join['kindergartens']=>$id, $this->tables['users_groups'].'.'.$this->join['groups'] => 2))
            ->join($this->tables['users_groups'], $this->tables['users_groups'].'.'.$this->join['users'].'='.$this->tables['users'].'.id')
            ->get($this->tables['users']);
    }

    /**
     * update
     *
     * @return bool
     * @author Phil Sturgeon
     **/
    public function update($id, array $data)
    {
        $this->trigger_events('pre_update_user');

        $user = $this->user($id)->row();

        $this->db->trans_begin();

        if (array_key_exists($this->identity_column, $data) && $this->identity_check($data[$this->identity_column]) && $user->{$this->identity_column} !== $data[$this->identity_column])
        {
            $this->db->trans_rollback();
            $this->set_error('account_creation_duplicate_'.$this->identity_column);

            $this->trigger_events(array('post_update_user', 'post_update_user_unsuccessful'));
            $this->set_error('update_unsuccessful');

            return FALSE;
        }

        // Filter the data passed
        $data = $this->_filter_data($this->tables['users'], $data);

        if (array_key_exists('username', $data) || array_key_exists('password', $data) || array_key_exists('email', $data))
        {
            if (array_key_exists('password', $data))
            {
                if( ! empty($data['password']))
                {
                    $data['password'] = $this->hash_password($data['password'], $user->salt);
                }
                else
                {
                    // unset password so it doesn't effect database entry if no password passed
                    unset($data['password']);
                }
            }
        }

        $this->trigger_events('extra_where');
        $this->db->update($this->tables['users'], $data, array('id' => $user->id));

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            $this->trigger_events(array('post_update_user', 'post_update_user_unsuccessful'));
            $this->set_error('update_unsuccessful');
            return FALSE;
        }

        $this->db->trans_commit();

        $this->trigger_events(array('post_update_user', 'post_update_user_successful'));
        $this->set_message('update_successful');
        return TRUE;
    }

    /**
     * delete_user
     *
     * @return bool
     * @author Phil Sturgeon
     **/
    public function delete_kindergarten($id)
    {
        $this->trigger_events('pre_delete_kindergarten');

        $this->db->trans_begin();

        // delete kindergarten from kindergartens table should be placed
        $this->db->delete($this->tables['kindergartens'], array('id' => $id));

        // if user does not exist in database then it returns FALSE else removes the user from groups
        if ($this->db->affected_rows() == 0)
        {
            return FALSE;
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->trigger_events(array('post_delete_kindergarten', 'post_delete_kindergarten_unsuccessful'));
            $this->set_error('delete_kindergarten_unsuccessful');
            return FALSE;
        }

        $this->db->trans_commit();

        $this->trigger_events(array('post_delete_kindergarten', 'post_delete_kindergarten_successful'));
        $this->set_message('delete_kindergarten_successful');
        return TRUE;
    }

    /**
     * Checks name
     *
     * @return bool
     **/
    public function name_check($name = '')
    {
        $this->trigger_events('name_check');

        if (empty($name))
        {
            return FALSE;
        }

        $this->trigger_events('extra_where');

        return $this->db->where('name', $name)
            ->order_by("id", "ASC")
            ->limit(1)
            ->count_all_results($this->tables['categories']) > 0;
    }

    public function set_hook($event, $name, $class, $method, $arguments)
    {
        $this->_hooks->{$event}[$name] = new stdClass;
        $this->_hooks->{$event}[$name]->class     = $class;
        $this->_hooks->{$event}[$name]->method    = $method;
        $this->_hooks->{$event}[$name]->arguments = $arguments;
    }

    public function remove_hook($event, $name)
    {
        if (isset($this->_hooks->{$event}[$name]))
        {
            unset($this->_hooks->{$event}[$name]);
        }
    }

    public function remove_hooks($event)
    {
        if (isset($this->_hooks->$event))
        {
            unset($this->_hooks->$event);
        }
    }

    protected function _call_hook($event, $name)
    {
        if (isset($this->_hooks->{$event}[$name]) && method_exists($this->_hooks->{$event}[$name]->class, $this->_hooks->{$event}[$name]->method))
        {
            $hook = $this->_hooks->{$event}[$name];

            return call_user_func_array(array($hook->class, $hook->method), $hook->arguments);
        }

        return FALSE;
    }

    public function trigger_events($events)
    {
        if (is_array($events) && !empty($events))
        {
            foreach ($events as $event)
            {
                $this->trigger_events($event);
            }
        }
        else
        {
            if (isset($this->_hooks->$events) && !empty($this->_hooks->$events))
            {
                foreach ($this->_hooks->$events as $name => $hook)
                {
                    $this->_call_hook($events, $name);
                }
            }
        }
    }

    public function limit($limit)
    {
        $this->trigger_events('limit');
        $this->_limit = $limit;

        return $this;
    }

    public function offset($offset)
    {
        $this->trigger_events('offset');
        $this->_offset = $offset;

        return $this;
    }

    public function where($where, $value = NULL)
    {
        $this->trigger_events('where');

        if (!is_array($where))
        {
            $where = array($where => $value);
        }

        array_push($this->_where, $where);

        return $this;
    }

    public function like($like, $value = NULL, $position = 'both')
    {
        $this->trigger_events('like');

        if (!is_array($like))
        {
            $like = array($like => array(
                'value'    => $value,
                'position' => $position,
            ));
        }

        array_push($this->_like, $like);

        return $this;
    }

    public function select($select)
    {
        $this->trigger_events('select');

        $this->_select[] = $select;

        return $this;
    }

    public function order_by($by, $order='desc')
    {
        $this->trigger_events('order_by');

        $this->_order_by = $by;
        $this->_order    = $order;

        return $this;
    }

    public function row()
    {
        $this->trigger_events('row');

        $row = $this->response->row();

        return $row;
    }

    public function row_array()
    {
        $this->trigger_events(array('row', 'row_array'));

        $row = $this->response->row_array();

        return $row;
    }

    public function result()
    {
        $this->trigger_events('result');

        $result = $this->response->result();

        return $result;
    }

    public function result_array()
    {
        $this->trigger_events(array('result', 'result_array'));

        $result = $this->response->result_array();

        return $result;
    }

    public function num_rows()
    {
        $this->trigger_events(array('num_rows'));

        $result = $this->response->num_rows();

        return $result;
    }

    /**
     * set_message_delimiters
     *
     * Set the message delimiters
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function set_message_delimiters($start_delimiter, $end_delimiter)
    {
        $this->message_start_delimiter = $start_delimiter;
        $this->message_end_delimiter   = $end_delimiter;

        return TRUE;
    }

    /**
     * set_error_delimiters
     *
     * Set the error delimiters
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function set_error_delimiters($start_delimiter, $end_delimiter)
    {
        $this->error_start_delimiter = $start_delimiter;
        $this->error_end_delimiter   = $end_delimiter;

        return TRUE;
    }

    /**
     * set_message
     *
     * Set a message
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function set_message($message)
    {
        $this->messages[] = $message;

        return $message;
    }

    /**
     * messages
     *
     * Get the messages
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function messages()
    {
        $_output = '';
        foreach ($this->messages as $message)
        {
            $messageLang = $this->lang->line($message) ? $this->lang->line($message) : '##' . $message . '##';
            $_output .= $this->message_start_delimiter . $messageLang . $this->message_end_delimiter;
        }

        return $_output;
    }

    /**
     * messages as array
     *
     * Get the messages as an array
     *
     * @return array
     * @author Raul Baldner Junior
     **/
    public function messages_array($langify = TRUE)
    {
        if ($langify)
        {
            $_output = array();
            foreach ($this->messages as $message)
            {
                $messageLang = $this->lang->line($message) ? $this->lang->line($message) : '##' . $message . '##';
                $_output[] = $this->message_start_delimiter . $messageLang . $this->message_end_delimiter;
            }
            return $_output;
        }
        else
        {
            return $this->messages;
        }
    }

    /**
     * set_error
     *
     * Set an error message
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function set_error($error)
    {
        $this->errors[] = $error;

        return $error;
    }

    /**
     * errors
     *
     * Get the error message
     *
     * @return void
     * @author Ben Edmunds
     **/
    public function errors()
    {
        $_output = '';
        foreach ($this->errors as $error)
        {
            $errorLang = $this->lang->line($error) ? $this->lang->line($error) : '##' . $error . '##';
            $_output .= $this->error_start_delimiter . $errorLang . $this->error_end_delimiter;
        }

        return $_output;
    }

    /**
     * errors as array
     *
     * Get the error messages as an array
     *
     * @return array
     * @author Raul Baldner Junior
     **/
    public function errors_array($langify = TRUE)
    {
        if ($langify)
        {
            $_output = array();
            foreach ($this->errors as $error)
            {
                $errorLang = $this->lang->line($error) ? $this->lang->line($error) : '##' . $error . '##';
                $_output[] = $this->error_start_delimiter . $errorLang . $this->error_end_delimiter;
            }
            return $_output;
        }
        else
        {
            return $this->errors;
        }
    }

    protected function _filter_data($table, $data)
    {
        $filtered_data = array();
        $columns = $this->db->list_fields($table);

        if (is_array($data))
        {
            foreach ($columns as $column)
            {
                if (array_key_exists($column, $data))
                    $filtered_data[$column] = $data[$column];
            }
        }

        return $filtered_data;
    }
}
