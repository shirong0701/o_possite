
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by IntelliJ IDEA.
 * User: Tiger
 * Date: 2/2/2015
 * Time: 5:06 PM
 */


class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth','form_validation'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('main/main_view', 'refresh');
        }
        elseif ($this->ion_auth->is_master()) //remove this elseif if you want to enable this for non-admins
        {
            //redirect them to the home page because they must be an administrator to view this
//            redirect('kindergarten','refresh');
            $this->load->view('dashboard');
        }
        elseif ($this->ion_auth->is_admin())
        {
            $this->load->view('dashboard');
        }
    }

    function _remap( $method, $params )
    {
        $param 				= array();
        $param['control'] 	= $this->router->fetch_class();
        $param['action'] 	= $this->router->fetch_method();

        $this->load->view('header', $param);

        if( $method == "main_view" )
        {
            $this->load->view( 'main_view', $param );
        }
        else if( $method == "sign_in" )
        {
            if (!$this->ion_auth->logged_in())
            {
                //redirect them to the login page
                redirect('auth/login', 'refresh');
            }

            call_user_func_array( array( $this, $method ), $params );
        }
        else
        {
            if (method_exists($this, $method))
            {
				$this->load->view('sidebar', $param);

                call_user_func_array(array($this, $method), $params);
            }
            else
            {
                show_404();
            }
        }
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */