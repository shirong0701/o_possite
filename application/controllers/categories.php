<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Name:  Category Controller
 *
 * Author:  Tiger
 *
 * Created:  02.05.2015 10:23 AM
 *
 * Requirements: PHP5 or above
 *
 */

class Categories extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));

        $this->load->model('category_model');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'category'));

        $this->lang->load( 'auth' );
        $this->lang->load( 'category' );

        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        }
    }


    function _remap($method, $params)
    {
        $param = array();
        $param['control'] = $this->router->fetch_class();
        $param['action'] = $this->router->fetch_method();

        if ($method == 'delete_category') {
            show_404();
        } else {
            if (method_exists($this, $method)) {
                $this->load->view('header', $param);
                $this->load->view('sidebar', $param);
                call_user_func_array( array( $this, $method ), $params );
            } else {
                show_404();
            }
        }
    }

    //redirect if needed, otherwise display the user list
    function index()
    {
        if ( !$this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the categories
            $this->data['categories'] = $this->category_model->categories()->result();

        }
        $this->load->view( 'category/category_list', $this->data );
    }

    function create()
    {
        $this->data['title'] = $this->lang->line('create_category_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $admin_user = $this->ion_auth->user()->row();

        //validate form input
        $this->form_validation->set_rules('category_name', $this->lang->line('create_category_validation_name_label'), 'required|alpha_dash|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            $additional_data = array(
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone')
            );
            $new_category_id = $this->category_model->create_category($admin_user->kindergarten_id, $this->input->post('category_name'), $this->input->post('description'), $additional_data);
            if($new_category_id)
            {
                // check to see if we are creating the category
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->category_model->messages());
                redirect("category", 'refresh');
            }
        }
        else
        {
            //display the create category form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->category_model->errors() ? $this->category_model->errors() : $this->session->flashdata('message')));

            $this->data['category_name'] = array(
                'name'  => 'category_name',
                'id'    => 'category_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('category_name'),
            );
            $this->data['description'] = array(
                'name'  => 'description',
                'id'    => 'description',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('description'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );

            $this->load->view('category/create_category', $this->data);
        }
    }

    function edit($id)
    {
        // bail if no category id given
        if(!$id || empty($id))
        {
            redirect('category', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_category_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $category = $this->category_model->category($id)->row();

        //validate form input
        $this->form_validation->set_rules('category_name', $this->lang->line('edit_category_validation_name_label'), 'required|alpha_dash|xss_clean');
        $this->form_validation->set_rules('category_description', $this->lang->line('edit_category_validation_desc_label'), 'xss_clean');
        $this->form_validation->set_rules('email', $this->lang->line('edit_category_validation_email_label'), 'xss_clean|valid_email');
        $this->form_validation->set_rules('phone', $this->lang->line('edit_category_validation_phone_label'), 'xss_clean');

        if (isset($_POST) && !empty($_POST))
        {
            if ($this->form_validation->run() === TRUE)
            {
                $additional_data = array(
                    'description' => $this->input->post('category_description'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone')
                );

                $category_update = $this->category_model->update_category($id, $_POST['category_name'], $additional_data);

                if($category_update)
                {
                    $this->session->set_flashdata('message', $this->lang->line('edit_category_saved'));
                }
                else
                {
                    $this->session->set_flashdata('message', $this->category_model->errors());
                }
                redirect("category", 'refresh');
            }
        }

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->category_model->errors() ? $this->category_model->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['category'] = $category;

        $this->data['category_name'] = array(
            'name'  => 'category_name',
            'id'    => 'category_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('category_name', $category->name),
        );
        $this->data['category_description'] = array(
            'name'  => 'category_description',
            'id'    => 'category_description',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('category_description', $category->description),
        );

        $this->data['email'] = array(
            'name'  => 'email',
            'id'    => 'email',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('email', $category->email),
        );

        $this->data['phone'] = array(
            'name'  => 'phone',
            'id'    => 'phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('phone', $category->phone),
        );

        $this->load->view('category/edit_category', $this->data);
    }

    function delete_category($id)
    {
        if(!$id || empty($id))
        {
            redirect('category', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_category_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $category = $this->category_model->category($id)->row();

        if ( $category )
        {
            $this->category_model->delete_category($category->id);
            redirect('category', 'refresh');
        }
        else
        {
            redirect('category', 'refresh');
        }
    }

    function _render_page($view, $data=null, $render=false)
    {
        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $render);

        if (!$render) return $view_html;
    }
}