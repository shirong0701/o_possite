<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	function alert_test()
	{

	}

	//redirect if needed, otherwise display the Staff list
	function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page

			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('auth/index', $this->data);
		}
	}

	function _remap($method, $params) {
		$param 				= array();
		$param['control'] 	= $this->router->fetch_class();
		$param['action'] 	= $this->router->fetch_method();
		//$param['options'] 	= $this->option_model->options();

//		if( $method == "create_staff" )
//		{
//			$this->load->view('header', $param);
//
//			call_user_func_array(array($this, $method), $params);
//		}
//		else
 		if( $method == "master_login" || $method == "staff_login" || $method == "forgot_password" || $method == "reset_password" ) {
			if (method_exists($this, $method))
			{
				call_user_func_array(array($this, $method), $params);
			}
			else
			{
				show_404();
			}
		} else {

			if (method_exists($this, $method))
			{
//				if ( $this->ion_auth->is_master() ){
//					$this->load->view('kindergarten/header', $param);
//					$this->load->view('kindergarten/sidebar', $param);
//				}
//				else if ( $this->ion_auth->is_admin() ){
					$this->load->view('header', $param);
					$this->load->view('sidebar', $param);
//				}

				call_user_func_array(array( $this, $method ), $params );
			}
			else
			{
				show_404();
			}
		}
	}

	//log the master user in
	function master_login()
	{
		$this->load->view( 'header' );
		$this->data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('/', 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'email',
				'class' => 'form-control',
				'placeholder' => 'E-mail',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'placeholder' => 'Password',
				'class' => 'form-control',
			);

			$this->_render_page('auth/master_login', $this->data);
		}
	}

	//log the master user in
	function staff_login()
	{
		$this->load->view( 'header' );
		$this->data['title'] = "Staff Log In";

		//validate form input
		$this->form_validation->set_rules('code1', $this->lang->line('create_user_validation_code1_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code2', $this->lang->line('create_user_validation_code2_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code3', $this->lang->line('create_user_validation_code3_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code4', $this->lang->line('create_user_validation_code4_label'), 'required|min_length[1]|max_length[1]');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('/', 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/staff_login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['code1'] = array(
				'name'  => 'code1',
				'id'    => 'code1',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code1'),
			);
			$this->data['code2'] = array(
				'name'  => 'code2',
				'id'    => 'code2',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code2'),
			);
			$this->data['code3'] = array(
				'name'  => 'code3',
				'id'    => 'code3',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code3'),
			);
			$this->data['code4'] = array(
				'name'  => 'code4',
				'id'    => 'code4',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code4'),
			);

			$this->_render_page('auth/staff_login', $this->data);
		}
	}

	//log the user out
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('/', 'refresh');
	}

	//change password
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('auth/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password()
	{
		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_username_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth/forgot_password', $this->data);
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
			}
	            	if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') == 'username')
		            	{
                                   $this->ion_auth->set_message('forgot_password_username_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_message('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->messages());
                		redirect("auth/forgot_password", 'refresh');
            		}

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->_render_page('auth/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	function staff_list()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
//		elseif ($this->ion_auth->is_master()) //remove this elseif if you want to enable this for non-admins
//		{
//			//redirect them to the home page because they must be an administrator to view this
//			redirect('kindergarten','refresh');
//		}
		elseif (!$this->ion_auth->is_master())  //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users( array( 2 ) )->result();

			$this->_render_page('auth/staff_list', $this->data);
		}
	}


	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_master())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ( $activation )
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
//			redirect("auth", 'refresh');
		}
		else {
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
		}
	}

	//deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_master())
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$this->ion_auth->deactivate($id);

		/*
		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_master())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect('auth', 'refresh');
		}*/
	}

	//create a new user
	function create_staff()
	{
		$this->data['title'] = "Create Staff";

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_master())
		{
			redirect('auth', 'refresh');
		}
		$tables = $this->config->item('tables','ion_auth');

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('code1', $this->lang->line('create_user_validation_code1_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code2', $this->lang->line('create_user_validation_code2_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code3', $this->lang->line('create_user_validation_code3_label'), 'required|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('code4', $this->lang->line('create_user_validation_code4_label'), 'required|min_length[1]|max_length[1]');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('code1').$this->input->post('code2').$this->input->post('code3').$this->input->post('code4');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'phone'      => $this->input->post('phone'),
				'code'       => $password
			);
		}
		if ($this->form_validation->run() == true && $new_user_id = $this->ion_auth->register($username, $password, $email, $additional_data, array(2)))
		{
			if ( isset($_FILES['avatar_file']['name']) && !empty($_FILES['avatar_file']['name']) )
			{
				$config['upload_path'] = './uploads/avatars/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '10000';
				$config['max_width']  = '0';
				$config['max_height']  = '0';

				$ext = end( ( explode( ".", $_FILES['avatar_file']['name'] ) ) );
				$config['file_name']	=	$new_user_id.".".$ext;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('avatar_file'))
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());
				}
				else
				{
					$photo_data = array('upload_data' => $this->upload->data());
					$photo_imageurl = base_url('uploads/avatars/'.$photo_data['upload_data']['file_name']);
					$photo_thumburl = "";

					$image_config['image_library'] = 'gd2';
					$image_config['source_image']	= $photo_data['upload_data']['full_path'];
					$image_config['create_thumb'] = TRUE;
					$image_config['maintain_ratio'] = TRUE;
					$image_config['width']	= 100;
					$image_config['height']	= 100;

					$this->load->library('image_lib', $image_config);
					if ( $this->image_lib->resize() )
					{
						$photo_thumburl = base_url('uploads/avatars/'.$photo_data['upload_data']['raw_name']."_thumb".$photo_data['upload_data']['file_ext']);
					}
					else
					{
						$this->session->set_flashdata('message', $this->image_lib->display_errors());
					}

					$this->ion_auth->add_avatar( $new_user_id, $photo_imageurl, $photo_thumburl );
				}
			}

			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth/create_staff", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['photo_file'] = array(
				'name'  => 'photo_file',
				'id'    => 'photo_file',
				'value' => '',
			);
			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['phone'] = array(
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['code1'] = array(
				'name'  => 'code1',
				'id'    => 'code1',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code1'),
			);
			$this->data['code2'] = array(
				'name'  => 'code2',
				'id'    => 'code2',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code2'),
			);
			$this->data['code3'] = array(
				'name'  => 'code3',
				'id'    => 'code3',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code3'),
			);
			$this->data['code4'] = array(
				'name'  => 'code4',
				'id'    => 'code4',
				'type'  => 'number',
				'value' => $this->form_validation->set_value('code4'),
			);
			$this->data['photo_file'] = array(
				'name'  => 'photo_file',
				'id'    => 'photo_file',
				'value' => '',
			);

			$this->_render_page('auth/create_staff', $this->data);
		}
	}

	//edit a staff
	function edit_staff( $id )
	{
		$this->data['title'] = "Edit User";


		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_master() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user( $id )->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('code1', $this->lang->line('edit_user_validation_code1_label'), 'required|xss_clean');
		$this->form_validation->set_rules('code2', $this->lang->line('edit_user_validation_code2_label'), 'required|xss_clean');
		$this->form_validation->set_rules('code3', $this->lang->line('edit_user_validation_code3_label'), 'required|xss_clean');
		$this->form_validation->set_rules('code4', $this->lang->line('edit_user_validation_code4_label'), 'required|xss_clean');

		if ( isset( $_POST ) && !empty( $_POST ) )
		{
			// do we have a valid request?
			/*
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post( 'id' ) )
			{
				show_error($this->lang->line('error_csrf'));
			}*/

			//update the password if it was posted
			/*
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}*/

			if ($this->form_validation->run() === TRUE)
			{
				$photo_imageurl = "";
				$photo_thumburl = "";

				if ( isset($_FILES['avatar_file']['name']) && !empty($_FILES['avatar_file']['name']) )
				{
					$config['upload_path'] = './uploads/avatars/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite']	= true;

					$ext = end( ( explode( ".", $_FILES['avatar_file']['name'] ) ) );
					$config['file_name']	=	$id.".".$ext;

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload('avatar_file'))
					{
						$this->session->set_flashdata('message', $this->upload->display_errors());
					}
					else
					{
						$photo_data = array('upload_data' => $this->upload->data());
						$photo_imageurl = base_url('uploads/avatars/'.$photo_data['upload_data']['file_name']);
						$photo_thumburl = "";

						$image_config['image_library'] = 'gd2';
						$image_config['source_image']	= $photo_data['upload_data']['full_path'];
						$image_config['create_thumb'] = TRUE;
						$image_config['maintain_ratio'] = TRUE;
						$image_config['width']	= 100;
						$image_config['height']	= 100;

						$this->load->library('image_lib', $image_config);
						if ( $this->image_lib->resize() )
						{
							$photo_thumburl = base_url('uploads/avatars/'.$photo_data['upload_data']['raw_name']."_thumb".$photo_data['upload_data']['file_ext']);
						}
						else
						{
							$this->session->set_flashdata('message', $this->image_lib->display_errors());
						}
					}
				}

				$avatar_changed = $this->input->post( 'avatar_changed' );

				if( $avatar_changed == "true" ) {
					$this->ion_auth->add_avatar($id, $photo_imageurl, $photo_thumburl );
				}

				$data['password'] = $this->input->post('code1').$this->input->post('code2').$this->input->post('code3').$this->input->post('code4');

				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'phone'      => $this->input->post('phone'),
					'code'		 => $this->input->post('code1').$this->input->post('code2').$this->input->post('code3').$this->input->post('code4')
				);

				//check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_master())
					{
						redirect('auth/staff_list', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}
			    }
			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['avatar'] = array(
			'name'  => 'avatar',
			'id'    => 'avatar',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('avatar', $user->avatar),
		);
		$this->data['avatar_thumb'] = array(
			'name'  => 'avatar_thumb',
			'id'    => 'avatar_thumb',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('avatar_thumb', $user->avatar_thumb),
		);
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email', $user->email),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['code1'] = array(
			'name' => 'code1',
			'id'   => 'code1',
			'type' => 'text',
			'value'=> $this->form_validation->set_value('code1', substr($user->code, 0,1))
		);
		$this->data['code2'] = array(
			'name' => 'code2',
			'id'   => 'code2',
			'type' => 'text',
			'value'=> $this->form_validation->set_value('code2', substr($user->code, 1,1))
		);
		$this->data['code3'] = array(
			'name' => 'code3',
			'id'   => 'code3',
			'type' => 'text',
			'value'=> $this->form_validation->set_value('code2', substr($user->code, 2,1))
		);
		$this->data['code4'] = array(
			'name' => 'code4',
			'id'   => 'code4',
			'type' => 'text',
			'value'=> $this->form_validation->set_value('code4', substr($user->code, 3,1))
		);

		$this->_render_page('auth/edit_staff', $this->data);
	}


	function delete_staff( $id )
	{

		$user = $this->ion_auth->user( $id )->row();
		if( $user )
			$this->ion_auth->delete_user( $user->id );
		redirect( 'auth/user_list', 'refresh' );
	}

	// create a new group
	function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	//edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['group'] = $group;

		$this->data['group_name'] = array(
			'name'  => 'group_name',
			'id'    => 'group_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_name', $group->name),
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $this->data);
	}

	function profile()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

/*		if ( $this->ion_auth->is_master() )
		{
			$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required|xss_clean');
			$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required|xss_clean');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');

			if ($this->form_validation->run() == true)
			{

			}
			else
			{
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

				$this->data['avatar'] = array(
					'name'  => 'avatar',
					'id'    => 'avatar',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('avatar', $user->avatar),
				);
				$this->data['avatar_thumb'] = array(
					'name'  => 'avatar_thumb',
					'id'    => 'avatar_thumb',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('avatar_thumb', $user->avatar_thumb),
				);
				$this->data['first_name'] = array(
					'name'  => 'first_name',
					'id'    => 'first_name',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('first_name', $user->first_name),
				);
				$this->data['last_name'] = array(
					'name'  => 'last_name',
					'id'    => 'last_name',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('last_name', $user->last_name),
				);
				$this->data['email'] = array(
					'name'  => 'email',
					'id'    => 'email',
					'type'  => 'email',
					'value' => $this->form_validation->set_value('email', $user->email),
				);

				$this->load->view('kindergarten/profile', $this->data);
			}
		}
		else */if ($this->ion_auth->is_admin() || $this->ion_auth->is_master() )
		{
			$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required|xss_clean');
			$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required|xss_clean');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
			$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');

			$photo_imageurl = "";
			$photo_thumburl = "";

			if ($this->form_validation->run() == true)
			{
				$id = $this->session->userdata( 'user_id' );

				if ( isset($_FILES['avatar_file']['name']) && !empty($_FILES['avatar_file']['name']) )
				{
					$config['upload_path'] = './uploads/avatars/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite']	= true;

					$ext = end( ( explode( ".", $_FILES['avatar_file']['name'] ) ) );
					$config['file_name']	=	$id.".".$ext;

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload('avatar_file'))
					{
						$this->session->set_flashdata('message', $this->upload->display_errors());
					}
					else
					{
						$photo_data = array('upload_data' => $this->upload->data());
						$photo_imageurl = base_url('uploads/avatars/'.$photo_data['upload_data']['file_name']);
						$photo_thumburl = "";

						$image_config['image_library'] = 'gd2';
						$image_config['source_image']	= $photo_data['upload_data']['full_path'];
						$image_config['create_thumb'] = TRUE;
						$image_config['maintain_ratio'] = TRUE;
						$image_config['width']	= 100;
						$image_config['height']	= 100;

						$this->load->library('image_lib', $image_config);
						if ( $this->image_lib->resize() )
						{
							$photo_thumburl = base_url('uploads/avatars/'.$photo_data['upload_data']['raw_name']."_thumb".$photo_data['upload_data']['file_ext']);
						}
						else
						{
							$this->session->set_flashdata('message', $this->image_lib->display_errors());
						}
					}
				}

				$data = array(
					'first_name' 	=>		$this->input->post( 'first_name' ),
					'last_name'		=>		$this->input->post( 'last_name' ),
					'email'			=>		$this->input->post( 'email' ),
					'phone'			=>		$this->input->post( 'phone' )
				);

				$change = $this->ion_auth->update($id, $data );

				$avatar_changed = $this->input->post( 'avatar_changed' );

				if( $avatar_changed == "true" ) {
					$this->ion_auth->add_avatar($id, $photo_imageurl, $photo_thumburl);
				}

				if ( $change )
				{
//					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect('auth/profile', 'refresh');
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('auth/change_password', 'refresh');
				}
			}
			else
			{
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

				$this->data['avatar'] = array(
					'name'  => 'avatar',
					'id'    => 'avatar',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('avatar', $user->avatar),
				);
				$this->data['avatar_thumb'] = array(
					'name'  => 'avatar_thumb',
					'id'    => 'avatar_thumb',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('avatar_thumb', $user->avatar_thumb),
				);
				$this->data['first_name'] = array(
					'name'  => 'first_name',
					'id'    => 'first_name',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('first_name', $user->first_name),
				);
				$this->data['last_name'] = array(
					'name'  => 'last_name',
					'id'    => 'last_name',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('last_name', $user->last_name),
				);
				$this->data['email'] = array(
					'name'  => 'email',
					'id'    => 'email',
					'type'  => 'email',
					'value' => $this->form_validation->set_value('email', $user->email),
				);
				$this->data['phone'] = array(
					'name'  => 'phone',
					'id'    => 'phone',
					'type'  => 'phone',
					'value' => $this->form_validation->set_value('phone', $user->phone),
				);

				$param['control'] 	= $this->router->fetch_class();
				$param['action'] 	= $this->router->fetch_method();

				$this->load->view('auth/profile', $this->data);
			}
		}
	}

	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $render=false)
	{
		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}
}
