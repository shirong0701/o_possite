<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Category
 *
 * Author: Tiger
 *
 * Created:  02.05.2015 7:23 AM
 *
 * Requirements: PHP5 or above
 *
 */

/*
| -------------------------------------------------------------------------
| Tables.
| -------------------------------------------------------------------------
| Database table names.
*/
// Create Category
$lang['create_category_title']                          = 'Create Category';
$lang['create_category_heading']                        = 'Create Category';
$lang['create_category_subheading']                     = 'Please enter the Category information below.';
$lang['create_category_name_label']                     = 'Category Name:';
$lang['create_category_submit_btn']                     = 'Create Category';
$lang['create_category_validation_name_label']          = 'Category Name';
$lang['create_category_validation_desc_label']          = 'Description';

// Index
$lang['index_heading']                                  = 'Categories';
$lang['index_subheading']                               = 'Below is a list of the categories.';
$lang['index_name_th']                                  = 'Name';
$lang['index_items_count_th']                           = 'Items Count';
$lang['index_action_th']                                = 'Action';
$lang['index_create_user_link']                         = 'Create a new user';
$lang['index_create_group_link']                        = 'Create a new group';

// Edit Category
$lang['edit_category_title']                            = 'Edit Category';
$lang['edit_category_saved']                            = 'Category Saved';
$lang['edit_category_heading']                          = 'Edit Category';
$lang['edit_category_subheading']                       = 'Please enter the Category information below.';
$lang['edit_category_name_label']                       = 'Category Name:';
$lang['edit_category_submit_btn']                       = 'Save Category';
$lang['edit_category_validation_name_label']            = 'Category Name';

// Categories
$lang['Category_creation_successful']                   = 'Category created Successfully';
$lang['Category_already_exists']                        = 'Category name already taken';
$lang['Category_update_successful']                     = 'Category details updated';
$lang['Category_delete_successful']                     = 'Category deleted';
$lang['Category_delete_unsuccessful'] 	                = 'Unable to delete Category';
$lang['Category_name_required'] 		                = 'Category name is a required field';