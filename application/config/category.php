<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Department
 *
 * Author: Tiger
 *
 * Created:  02.05.2015 7:23 AM
 *
 * Requirements: PHP5 or above
 *
 */

/*
| -------------------------------------------------------------------------
| Tables.
| -------------------------------------------------------------------------
| Database table names.
*/
$config['tables']['categories']            = 'categories';
/*
 | Users table column and Group table column you want to join WITH.
 |
 | Joins from users.id
 | Joins from groups.id
 */
$config['join']['items']        = 'item_id';
$config['join']['departments']  = 'department_id';

/*
 | -------------------------------------------------------------------------
 | Message Delimiters.
 | -------------------------------------------------------------------------
 */
$config['delimiters_source']       = 'config'; 	// "config" = use the settings defined here, "form_validation" = use the settings defined in CI's form validation library
$config['message_start_delimiter'] = '<div class="alert alert-success">'; 	// Message start delimiter
$config['message_end_delimiter']   = '</div>'; 	// Message end delimiter
$config['error_start_delimiter']   = '<div class="alert alert-danger">';		// Error mesage start delimiter
$config['error_end_delimiter']     = '</div>';	// Error mesage end delimiter

/* End of file ion_auth.php */
/* Location: ./application/config/ion_auth.php */
