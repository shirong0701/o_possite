(function($) {

	"use strict";

	var options = {
		events_source: 'calendar/events',
		view: 'month',
		tmpl_path: asset_url + '/components/calendar/tmpls/',
		tmpl_cache: false,
		first_day: 1,
		onAfterEventsLoad: function(events) {
			if(!events) {
				return;
			}
			var list = $('#eventlist');
			list.html('');

			$.each(events, function(key, val) {
				$(document.createElement('li'))
					.html('<a href="' + val.url + '">' + val.title + '</a>')
					.appendTo(list);
			});
		},
		onAfterViewLoad: function(view) {
			$('.page-header h3').text(this.getTitle());
			$('.btn-group button').removeClass('active');
			$('button[data-calendar-view="' + view + '"]').addClass('active');
		},
		classes: {
			months: {
				general: 'label'
			}
		}
	};

	var calendar = $('#calendar').calendar(options);

	$('.btn-group button[data-calendar-nav]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.navigate($this.data('calendar-nav'));
		});
	});

	$('.btn-group button[data-calendar-view]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.view($this.data('calendar-view'));
		});
	});

	$('#first_day').change(function(){
		var value = $(this).val();
		value = value.length ? parseInt(value) : null;
		calendar.setOptions({first_day: value});
		calendar.view();
	});

	$('#language').change(function(){
		calendar.setLanguage($(this).val());
		calendar.view();
	});

	$('#events-in-modal').change(function(){
		var val = $(this).val();
		calendar.setOptions({modal: val});
	});
	$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
		//e.preventDefault();
		//e.stopPropagation();
	});


	var start_dp = $("#event_start_at").datetimepicker({format: 'YYYY-MM-DD HH:mm', minuteStepping:5});
	$("#event_end_at").datetimepicker({format: 'YYYY-MM-DD HH:mm', minuteStepping:5});

	start_dp.change(function(){
		var start_date = new Date($("#event_start_at").val());
		var end_dp = $("#event_end_at").data('DateTimePicker');
		end_dp.setMinDate(start_date);
		end_dp.setDate(new Date(start_date.getTime() + 60 * 60000));
	});

	$('.add-event-btn').click(function(){

		if (!calendar.getActiveDate())
		{
			alert('Please select date.');
			return false;
		}
		else
		{
			$("#event_end_at").val('');
			$("#event_start_at").val('');

			var calendar_date = calendar.getActiveDate();
			var start_dp1= $("#event_start_at").data("DateTimePicker");
			start_dp1.setDate(calendar_date);

			$("#event_title").val('');
			$("#event_description").val('');
			$("#error-text").hide();
			$("#myModal").modal('show');
		}

	});

	$("#add_event_form").submit(function(){
		var start = $("#event_start_at").val();
		var end = $("#event_end_at").val();
		var title = $("#event_title").val();
		var description = $("#event_description").val();

		var error_str = '';
		if (start == '')
		{
			error_str += "<p>Please input start date.</p>";
			$("#event_start_at").parent('.form-group').addClass('has-error');
		}
		if (end == '')
		{
			error_str += "<p>Please input end date.</p>";
			$("#event_end_at").parent('.form-group').addClass('has-error');
		}
		if (title == '')
		{
			error_str += "<p>Please input title of event.</p>";
			$("#event_title").parent('.form-group').addClass('has-error');
		}

		if (error_str != '')
		{
			$("#error-text").html(error_str);
			$("#error-text").fadeIn();
			setTimeout(function(){
				$("#error-text").fadeOut();
				$(".form-group").removeClass('has-error');
			}, 2000)
			return false;
		}
		else
		{
			return true;
		}
	});
}(jQuery));